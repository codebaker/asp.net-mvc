﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EShop.Entities.OfferElements;

namespace EShop.Entities
{
    public class Offer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }
        public bool available { get; set; }
        [MaxLength(300)]
        public string url { get; set; }
        public bool delivery { get; set; }
        [MaxLength(250)]
        public string name { get; set; }
        public string description { get; set; }
        [MaxLength(50)]
        public string model { get; set; }
        [MaxLength(50)]
        public string type { get; set; }

        public virtual Currency Currency { get; set; }
        public virtual Vendor Vendor { get; set; }
        public virtual PublisherInfo PublisherInfo { get; set; }

        public virtual ICollection<Category> Categories { get; set; }
        public virtual ICollection<Picture> Pictures { get; set; }
        public virtual ICollection<Param> Params { get; set; }
        public virtual ICollection<OrdernigTime> OrderingTimes { get; set; }
        public virtual ICollection<Barcode> Barcodes { get; set; }

        public virtual ICollection<UserOffers> Users { get; set; }
    }
}
