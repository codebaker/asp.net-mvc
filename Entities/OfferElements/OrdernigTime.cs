﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EShop.Entities.OfferElements
{
    public class OrdernigTime
    {
        [Key]
        public int orderingId { get; set; }
        public string value { get; set; }
    }
}
