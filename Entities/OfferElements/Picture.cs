﻿using System;

namespace EShop.Entities.OfferElements
{
    public class Picture
    {
        public int pictureId { get; set; }
        public string url { get; set; }
    }
}
