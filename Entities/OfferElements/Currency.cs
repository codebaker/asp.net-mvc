﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EShop.Entities.OfferElements
{
    public class Currency
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int currencyId { get; set; }
        [MaxLength(10)]
        public string value { get; set; }
        public double price { get; set; }

        [Required]
        public Offer offer { get; set; }
    }
}
