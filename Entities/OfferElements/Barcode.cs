﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EShop.Entities.OfferElements
{
    public class Barcode
    {
        public int barcodeId { get; set; }
        [MaxLength(50)]
        public string value { get; set; }
    }
}
