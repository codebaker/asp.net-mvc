﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EShop.Entities.OfferElements
{
    public class Param
    {
        public int paramId { get; set; }
        public string value { get; set; }
        [MaxLength(100)]
        public string name { get; set; }
        [MaxLength(30)]
        public string unit { get; set; }
    }
}
