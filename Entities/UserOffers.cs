﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EShop.Entities
{
    public class UserOffers
    {
        [Key, Column(Order = 0)]
        public int UserId { get; set; }
        [Key, Column(Order = 1)]
        public int OfferId { get; set; }

        public virtual User User { get; set; }
        public virtual Offer Offer { get; set; }
        [Required]
        [MaxLength(30)]
        public string Status { get; set; }
    }
}
