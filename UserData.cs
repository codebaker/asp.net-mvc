﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

using EShop.Entities;

namespace EShop
{
    public class SessionData
    {
        public string id { get; private set; }

        public SessionData()
        {
            id = "1";
        }
    }

    public class SessionUserData : SessionData
    {
        public UserData user { get; private set; }

        public SessionUserData(User userInfo)
        {
            if (userInfo == null)
                user = new UserData();
            else
                user = new UserData { id = userInfo.UserId, name = userInfo.FirstName, role = "admin" };
        }

        public SessionUserData()
        {
            user = new UserData();
        }

        /// <summary>
        /// Представление пользователя "по умолчанию"
        /// </summary>
        public static SessionData UnknownUser
        {
            get
            {
                return new SessionUserData();
            }
        }
    }

    public class UserData
    {
        public int id { get; set; }
        public string role { get; set; }
        public string name { get; set; }
    }
}