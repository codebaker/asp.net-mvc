﻿mainApp.controller('categoriesController', function ($scope, $http) {
    $http({ method: 'GET', url: 'api/categories' })
        .success(function (data, status) {
            $scope.categories = data;
            $scope.status = status;
        })
        .error(function (data, status) {
            window.alert(status);
    });
});