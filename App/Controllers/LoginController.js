﻿mainApp.controller('loginController', function ($scope, $rootScope, AuthenticationService, LoginService) {
    $scope.manualAuthenticateTrying = false;

    $scope.isAuthenticated = function () {
        return LoginService.isAuthenticated();
    }

    // Current user authentication
    $scope.manualLogin = function (credentials) {
        $scope.manualAuthenticateTrying = true;
        AuthenticationService.login(credentials);
    }
    
});
