﻿mainApp.service('AuthenticationService', ['$rootScope', '$cookieStore', 'AUTH_EVENTS', 'LoginService', function ($rootScope, $cookieStore, AUTH_EVENTS, LoginService) {
    $rootScope.currentUser = null;

    function setCurrentUser(user) {
        $rootScope.currentUser = user;
    };

    // Log in
    this.login = function (userInfo) {
        LoginService.login(userInfo).then(function (user) {
            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
            setCurrentUser(user);
        }, function () {
            $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
        });
    };

    // Log out
    this.logout = function () {
        $rootScope.currentUser = null;
        // Remove cookies
        $cookieStore.remove('AuthData');
        LoginService.logout();
        $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
    }

    // Check if user was logged in
    this.isAuthenticated = function () {
        return LoginService.isAuthenticated();
    }

    // Check if user has permission to access to page
    this.isAuthorized = function (authorizedRoles) {
        return LoginService.isAuthorized(authorizedRoles);
    }
}])