﻿mainApp.service('Session', function () {
    this.create = function (sessionId, userId, userRole) {
        this.id = sessionId;
        this.userId = userId;
        this.userRole = userRole;
    };

    this.createEmpty = function () {
        this.id = 0; // Maybe not null - get from server?
        this.userId = -1;
        this.userRole = 'all';
    };

    this.destroy = function () {
        this.id = null;
        this.userId = null;
        this.userRole = null;
    };

    return this;
})