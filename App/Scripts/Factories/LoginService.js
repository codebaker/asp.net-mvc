﻿mainApp.factory('LoginService', function ($http, $cookieStore, Session, USER_ROLES) {
    var loginService = {};

    loginService.login = function (credentials) {
        return $http.post('api/users', credentials).then(function (res) {

            // User isn't authenticated
            if (res.data == 'null') {
                Session.createEmpty();
                return null;
            }

            Session.create(res.data.id, res.data.user.id, res.data.user.role);
            // Add data to cookie storage
            $cookieStore.put('AuthData', credentials.login + ':' + credentials.pass);

            return res.data.user;
        });
    };

    loginService.logout = function () {
        Session.destroy();
    }

    loginService.isAuthenticated = function () {
        return (Session.userId > 0);
    };

    loginService.isAuthorized = function (authorizedRoles) {
        if (!angular.isArray(authorizedRoles)) {
            authorizedRoles = [authorizedRoles];
        }

        // Check if page is public
        if (authorizedRoles.indexOf(USER_ROLES.all) !== -1)
            return true;

        return (loginService.isAuthenticated() && authorizedRoles.indexOf(Session.userRole) !== -1);
    };

    return loginService;
})