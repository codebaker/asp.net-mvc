﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using EShop.Models;

namespace EShop.Controllers
{
    public class UsersController : ApiController
    {
        OzonDatabaseContext context = OzonDatabaseEntry.Instance;

        private enum UsersPermissions
        {
            user = 1,
            moderator = 2,
            admin = 10
        }

        public IEnumerable<int> GetUsersPermissions()
        {
            return null;
        }

        [HttpPost]
        public SessionData Login([FromBody] UserInfo userInfo)
        {
            if (userInfo == null || !ModelState.IsValid)
                return null;

            var result = from a in context.Users
                         where (string.Compare(a.Login, userInfo.login) == 0) && (string.Compare(a.Password, userInfo.pass) == 0)
                         select a;

            if (!result.Any())
                return null;

            return new SessionUserData(result.FirstOrDefault());
        }
    }
}